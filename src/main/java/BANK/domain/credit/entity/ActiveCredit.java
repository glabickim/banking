package BANK.domain.credit.entity;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.UUID;

import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Version;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotEmpty;

import BANK.config.persistance.converter.LocalDateTimePersistenceConverter;
import BANK.domain.credit.dto.ActiveCreditSnapshot;
import BANK.domain.credit.dto.CreditSnapshot;
import BANK.sharedkernel.exception.EntityInStateNewException;

/**
 *
 * @author Mateusz.Glabicki
 */
@Entity
public class ActiveCredit
   implements Serializable {

   private static final long serialVersionUID = 7972265007575707207L;

   @Id
   @GeneratedValue(strategy = GenerationType.AUTO)
   private Long id;

   @NotNull
   @Convert(converter = LocalDateTimePersistenceConverter.class)
   private LocalDateTime startDate;
   
   @Convert(converter = LocalDateTimePersistenceConverter.class)
   private LocalDateTime finishDate;

   private boolean active;
   
   @NotNull
   private Double value;
   
   @NotNull
   private Long userId;
   
   @NotNull
   private Long creditId;


   protected ActiveCredit() {
   }

   public ActiveCredit( Long creditId, Long userId, LocalDateTime finishDate, Double value) {
      this.startDate = LocalDateTime.now();
      this.finishDate = finishDate;
      this.active = true;
      this.creditId = creditId;
      this.userId = userId;
      this.value = value;
   }

   public void finish() {
      this.active = false;
      this.finishDate = LocalDateTime.now();
   }

   public ActiveCreditSnapshot toSnapshot() {
      if (id == null) {
         throw new EntityInStateNewException();
      }

      
      return new ActiveCreditSnapshot(id, startDate, finishDate, active, creditId,userId,value);
   }

}
