package BANK.web.restapi.logger;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;



@RestController
@RequestMapping(value = "/api/javascript",
   method = RequestMethod.POST)
@PreAuthorize("isAuthenticated()")
public class JavaScriptLogApi {

   private static final Logger LOGGER = LoggerFactory.getLogger(JavaScriptLogApi.class);


   @RequestMapping("/info")
   public void info(@RequestBody JavaScriptLog javaScriptLog) {
      LOGGER.info("Received JavaScript info. URL <{}> message <{}> cause <{}> stackTrace <{}>",
         javaScriptLog.getUrl(),
         javaScriptLog.getMessage(),
         javaScriptLog.getCause(),
         javaScriptLog.getStackTrace());
   }


   @RequestMapping("/warn")
   public void warn(@RequestBody JavaScriptLog javaScriptLog) {
      LOGGER.warn("Received JavaScript warn. URL <{}> message <{}> cause <{}> stackTrace <{}>",
         javaScriptLog.getUrl(),
         javaScriptLog.getMessage(),
         javaScriptLog.getCause(),
         javaScriptLog.getStackTrace());
   }


   @RequestMapping("/error")
   public void error(@RequestBody JavaScriptLog javaScriptLog) {
      LOGGER.error("Received JavaScript error. URL <{}> message <{}> cause <{}> stackTrace <{}>",
         javaScriptLog.getUrl(),
         javaScriptLog.getMessage(),
         javaScriptLog.getCause(),
         javaScriptLog.getStackTrace());
   }


   @RequestMapping("/debug")
   public void debug(@RequestBody JavaScriptLog javaScriptLog) {
      LOGGER.debug("Received JavaScript debug. URL <{}> message <{}> cause <{}> stackTrace <{}>",
         javaScriptLog.getUrl(),
         javaScriptLog.getMessage(),
         javaScriptLog.getCause(),
         javaScriptLog.getStackTrace());
   }
}
