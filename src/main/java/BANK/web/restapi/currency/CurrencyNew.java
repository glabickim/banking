/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package BANK.web.restapi.currency;

import java.time.LocalDateTime;

import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotEmpty;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

import BANK.web.converter.LocalDateTimeDeserializer;
import BANK.web.restapi.annotations.RangeOfTimestamp;


/**
 *
 * @author Mateusz
 */

public class CurrencyNew {
   @NotNull   
   private String symbol;

   public String getSymbol() {
      return symbol;
   }

   public void setSymbol(String symbol) {
      this.symbol = symbol;
   }
   
}
