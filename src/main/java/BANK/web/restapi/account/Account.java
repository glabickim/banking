package BANK.web.restapi.account;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.UUID;

import org.springframework.hateoas.ResourceSupport;

import BANK.domain.account.dto.AccountSnapshot;

import com.fasterxml.jackson.annotation.JsonView;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.*;

/**
 *
 * @author Mateusz.Glabicki
 */

public class Account
   extends ResourceSupport
   implements Serializable {

   private static final long serialVersionUID = 123364444898230931L;

   private final Long accountId;
   private final Long currencyId;
   private final boolean active;

   private final String iban;
   private final Double balance;
   private final LocalDateTime createdAt;
   
   public Account(AccountSnapshot accountSnapshot) {
      this.accountId = accountSnapshot.getId();
      this.createdAt = accountSnapshot.getCreatedAt();
      this.iban = accountSnapshot.getIban();
      this.balance = accountSnapshot.getBalance();
      this.currencyId = accountSnapshot.getCurrencyId();
      this.active = accountSnapshot.isActive();
   }
   
   public Long getAccountId() {
      return accountId;
   }

   public LocalDateTime getCreatedAt() {
      return createdAt;
   }

   public Long getCurrencyId() {
      return currencyId;
   }

   public String getIban() {
      return iban;
   }

   public Double getBalance() {
      return balance;
   }

   public boolean isActive() {
      return active;
   }
   
     
}
