(function () {
   'use strict';

   angular
           .module('bank')
           .controller('CurrencyCommonCtrl', CurrencyCommonCtrl);

   CurrencyCommonCtrl.$inject = ['$scope', 'currency'];

   function CurrencyCommonCtrl($scope, currency) {
      $scope.currency = currency;
      $scope.changeView = changeView;
      function changeView(state) {
         $scope.view = {
            details: false
         };
         
         switch (state) {
            case 'DETAILS':
               $scope.view.details = true;
               break;
            default:
               break;
         }
      }
   }
})();
