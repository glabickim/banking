package BANK.domain.currency.finder;

import java.util.List;
import java.util.Set;

import BANK.domain.currency.dto.CurrencySnapshot;

/**
 *
 * @author Mateusz.Glabicki
 */
public interface ICurrencySnapshotFinder {

   CurrencySnapshot findById(Long id);

   List<CurrencySnapshot> findAll(Set<Long> ids);

   List<CurrencySnapshot> findAll();

   CurrencySnapshot findBySymbol(String symbol);
}
