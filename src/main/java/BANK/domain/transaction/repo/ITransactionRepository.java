package BANK.domain.transaction.repo;

import java.time.LocalDateTime;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import BANK.domain.transaction.entity.Transaction;

/**
 *
 * @author Mateusz.Glabicki
 */
public interface ITransactionRepository
   extends JpaRepository<Transaction, Long> {

//   List<Transaction> findByFromIBAN(String name);
//   
//   List<Transaction> findByToIBAN(String name);
   
   List<Transaction> findByCreatedAtBetween(LocalDateTime before, LocalDateTime after);
}
