package BANK.domain.transaction.dto;

import java.time.LocalDateTime;

/**
 *
 * @author Mateusz.Glabicki
 */
public class TransactionSnapshot {

   private final Long id;
   
   private final Long currencyId;
   private final String fromIBAN;
   private final String toIBAN;
   private final Double value;
   private final LocalDateTime createdAt;

   public TransactionSnapshot(Long id, String fromIBAN, String toIBAN,Double value, LocalDateTime createdAt, Long currencyId) {

      this.id = id;
      this.fromIBAN = fromIBAN;
      this.toIBAN = toIBAN;
      this.value = value ; 
      this.currencyId = currencyId;
      this.createdAt = createdAt;
   }

   public Long getId() {
      return id;
   }

   public Long getCurrencyId() {
      return currencyId;
   }

   public String getFromIBAN() {
      return fromIBAN;
   }

   public String getToIBAN() {
      return toIBAN;
   }

   public Double getValue() {
      return value;
   }

   public LocalDateTime getCreatedAt() {
      return createdAt;
   }

   
   
}
