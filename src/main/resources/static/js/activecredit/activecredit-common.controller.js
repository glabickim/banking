(function () {
   'use strict';

   angular
           .module('bank')
           .controller('ActiveCreditCommonCtrl', ActiveCreditCommonCtrl);

   ActiveCreditCommonCtrl.$inject = ['$scope' ,'activeCredit','dateResolveUtil'];

   function ActiveCreditCommonCtrl($scope,activeCredit,dateResolveUtil) {
      $scope.activeCredit = activeCredit;
      $scope.onlyDate = dateResolveUtil.onlyDate;
      $scope.changeView = changeView;
      function changeView(state) {
         $scope.view = {
            details: false
         };
         
         switch (state) {
            case 'DETAILS':
               $scope.view.details = true;
               break;
            default:
               break;
         }
      }
   }
})();
