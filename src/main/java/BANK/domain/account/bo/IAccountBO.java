package BANK.domain.account.bo;

import BANK.domain.account.dto.AccountSnapshot;

/**
 *
 * @author Mateusz.Glabicki
 */
public interface IAccountBO {

   AccountSnapshot add(String iban, Double balance, Long currencyId,boolean active, Long userId);

   void delete(Long id);
   
   void active(Long id);
   
   AccountSnapshot boost(Long id,Double balance);

   void edit(Long id, String iban, Double balance, Long currencyId);
}
