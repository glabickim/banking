package BANK;

import java.io.IOException;
import java.util.Arrays;

import javax.annotation.PostConstruct;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.core.env.Environment;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.annotation.EnableScheduling;

/**
 * This class is used in Maven DEVELOPMENT profile to run easily Spring boot application. Also it can be used while
 * using JAR packaging.
 *
 * 
 */
@SpringBootApplication
@EnableScheduling
@EnableAsync
public class Application {

   public static void main(String[] args) {
      SpringApplication.run(Application.class, args);
   }

}
