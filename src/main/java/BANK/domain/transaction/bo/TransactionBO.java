package BANK.domain.transaction.bo;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.context.ApplicationEventPublisherAware;

import BANK.domain.transaction.dto.TransactionSnapshot;
import BANK.domain.transaction.entity.Transaction;
import BANK.domain.transaction.repo.ITransactionRepository;
import BANK.sharedkernel.annotations.BussinesObject;

@BussinesObject
public class TransactionBO
   implements ITransactionBO {

   private static final Logger LOGGER = LoggerFactory.getLogger(TransactionBO.class);

   private final ITransactionRepository transactionRepository;

   private ApplicationEventPublisher eventPublisher;

   @Autowired
   public TransactionBO(ITransactionRepository transactionRepository) {
      this.transactionRepository = transactionRepository;
   }

   @Override
   public TransactionSnapshot add(String fromIBAN, String toIBAN, Double value, Long currencyId) {
      Transaction transaction = new Transaction(fromIBAN, toIBAN, value, currencyId);
      transaction = transactionRepository.save(transaction);

      TransactionSnapshot transactionSnapshot = transaction.toTransactionSnapshot();


      LOGGER.info("Add Transaction <{}> <{}> <{}>",
         transactionSnapshot.getId(), transactionSnapshot.getFromIBAN(),transactionSnapshot.getToIBAN());

      return transactionSnapshot;
   }

   @Override
   public void delete(Long id) {
      Transaction transaction = transactionRepository.findOne(id);
      transactionRepository.delete(id);

      LOGGER.info("Delete Transaction <{}>", id);
   }


}
