(function () {
   'use strict';

   angular
           .module('bank')
           .controller('ActiveCreditAddCtrl', ActiveCreditAddCtrl);

   ActiveCreditAddCtrl.$inject = [
      '$scope',
      '$state',
      'credits',
      'ActiveCredit',
      'CommonUtilService',
      'logToServerUtil',
      'dateResolveUtil',
      'languageUtil',
      'popupService'];

   function ActiveCreditAddCtrl($scope, $state,credits, ActiveCredit, CommonUtilService, logToServerUtil,dateResolveUtil, languageUtil,popupService) {
      CommonUtilService.initCommonAndOperator($scope);

      $scope.credits = credits;
      $scope.creditType = 'house';

      $scope.activeCredit = {
         creditId: '',
         finishDate: '',
         value:''
      };
      
      $scope.onTimeSet = function (newDate, oldDate) {
         $scope.activeCredit.finishDate = dateResolveUtil.onlyDate(dateResolveUtil.formatToISODate(new Date($scope.activeCredit.finishDateCalendar)));
     }
      
      $scope.saveActiveCredit = saveActiveCredit;
      $scope.cancel = cancel;
      
      function cancel(){
          $state.go('user.common',{id:$state.params.userId});
      }

      function saveActiveCredit() {
         var activeCredit = new ActiveCredit();
         activeCredit.creditId = $scope.activeCredit.creditId;
         activeCredit.value = $scope.activeCredit.value;
         activeCredit.userId = $state.params.userId;
//         activeCredit.finishDate = $scope.activeCredit.finishDate;
         activeCredit.finishDate = dateResolveUtil.formatToISODate(new Date($scope.activeCredit.finishDate));
//         activeCredit.startDate = dateResolveUtil.formatToISODate($scope.activeCredit.startDate);
         
         activeCredit.$save()
                 .then(function (result) {
//                    $state.go('activeCredit.list');
                    popupService.success( 'activeCredit-add.popup.save.success.title','activeCredit-add.popup.save.success.body');
                    $state.go('user.common',{id:$state.params.userId});
                 }, function (reason) {
                    popupService.error( 'activeCredit-add.popup.save.failure.title','activeCredit-add.popup.save.failure.body');
                    logToServerUtil.trace('Save ActiveCredit failure', reason);
                 });
      }
   }
})();