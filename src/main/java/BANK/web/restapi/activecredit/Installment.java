package BANK.web.restapi.activecredit;

import java.io.Serializable;
import java.time.LocalDateTime;

import org.springframework.hateoas.ResourceSupport;

import BANK.domain.installment.dto.InstallmentSnapshot;


/**
 *
 * @author Mateusz.Glabicki
 */

public class Installment
   extends ResourceSupport
   implements Serializable {

   private static final long serialVersionUID = 123364444898230931L;

   private final Long installmentId;
   
   private final Long activeCreditId;

   private final LocalDateTime createdAt;
   
   private final LocalDateTime paiedAt;
   
   private final LocalDateTime finishDate;
   
   private final long version;
   
   private final Double amount;
   
   private final boolean active;
   
   public Installment(InstallmentSnapshot installmentSnapshot) {
      this.installmentId = installmentSnapshot.getId();
      this.createdAt = installmentSnapshot.getCreatedAt();
      this.paiedAt = installmentSnapshot.getPaiedAt();
      this.amount = installmentSnapshot.getAmount();
      this.active = installmentSnapshot.isActive();
      this.version = installmentSnapshot.getVersion();
      this.finishDate = installmentSnapshot.getFinishDate();
      this.activeCreditId = installmentSnapshot.getActiveCreditId();
   }

   public Long getInstallmentId() {
      return installmentId;
   }

   public LocalDateTime getCreatedAt() {
      return createdAt;
   }

   public LocalDateTime getPaiedAt() {
      return paiedAt;
   }

   public Double getAmount() {
      return amount;
   }

   public boolean isActive() {
      return active;
   }

   public LocalDateTime getFinishDate() {
      return finishDate;
   }

   public Long getActiveCreditId() {
      return activeCreditId;
   }
   
}
