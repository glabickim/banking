(function () {
   'use strict';

   angular
           .module('bank')
           .config(userProvider);

   userProvider.$inject = ['$stateProvider', '$breadcrumbProvider'];

   function userProvider($stateProvider, $breadcrumbProvider) {
      var resolveUserAccounts = ['$state', 'Myaccount','$stateParams', 'logToServerUtil', loadUserAccounts];
      var resolveUserActiveCredits = ['$state', 'ActiveCredit','$stateParams', 'logToServerUtil', loadUserActiveCredits];
      var resolveUsers = ['$state', 'User',  'logToServerUtil', loadUsers];
      var resolveCurrencys = ['$state', 'Currency', 'logToServerUtil', loadCurrencys];
      var resolveSingleUser = ['$state', '$stateParams', 'User', 'logToServerUtil',
         loadSingleUser];

      $stateProvider
              .state('user', {
                 parent: 'root',
                 url: '/user',
                 abstract: true,
                 template: '<ui-view />'
              })
              .state('user.list', {
                 url: '/list?lastname&firstname&title&enabled',
                 reloadOnSearch: false,
                 templateUrl: '/user/list',
                 controller: 'UserListCtrl',
                 resolve: {
                    users: resolveUsers,
                    translations: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                          $translatePartialLoader.addPart('user/list');
                          return $translate.refresh();
                       }]
                 }
              })
              .state('user.add', {
                 url: '/add',
                 reloadOnSearch: false,
                 templateUrl: '/user/add',
                 controller: 'UserAddCtrl',
                 resolve: {
                    translations: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                          $translatePartialLoader.addPart('user/add');
                          return $translate.refresh();
                       }]
                 }
              })
              .state('user.edit', {
                 url: '/edit/{id:int}',
                 reloadOnSearch: false,
                 templateUrl: '/user/edit',
                 controller: 'UserEditCtrl',
                 resolve: {
                    user: resolveSingleUser,
                    translations: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                          $translatePartialLoader.addPart('user/edit');
                          return $translate.refresh();
                       }]
                 }
              })
              .state('user.common', {
                 url: '/{id:int}',
                 templateUrl: '/user/common',
                 controller: 'UserCommonCtrl',
                 redirectTo: 'user.common.details',
                 resolve: {
                    user: resolveSingleUser,
                    translations: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                          $translatePartialLoader.addPart('user/common');
                          return $translate.refresh();
                       }]
                 }
              })
              .state('user.common.details', {
                 url: '/details',
                 templateUrl: '/user/details',
                 controller: 'UserDetailsCtrl',
                 resolve: {
                    activeCredits: resolveUserActiveCredits,
                    accounts:resolveUserAccounts,
                    currencys: resolveCurrencys,
                    translations: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                          $translatePartialLoader.addPart('user/details');
                          return $translate.refresh();
                       }]
                 }
              });

      function loadCurrencys($state, Currency, logToServerUtil) {
         var currencyPromise = Currency.query().$promise;
         currencyPromise
                 .then(function () {
                 }, function (reason) {
                    logToServerUtil.trace('get Myaccounts failed', reason);
//                    $state.go('dashboard');
                 });
         return currencyPromise;
      }

      function loadUsers($state, User, logToServerUtil) {
         var usersPromise = User.query().$promise;
         usersPromise
                 .then(function () {
                 }, function (reason) {
//                    logToServerUtil.trace('get Users failed', reason);
//                    popupService.error('shared.popup.general-failure.title',
//                            'shared.popup.general-failure.body');
                    $state.go('dashboard');
                 });
         return usersPromise;
      }
      
      function loadUserAccounts($state, Myaccount,$stateParams, logToServerUtil) {
         var myaccountsPromise = Myaccount.userList({userId: $stateParams.id}).$promise;
         myaccountsPromise
                 .then(function () {
                 }, function (reason) {
                    logToServerUtil.trace('get Myaccounts failed', reason);
//                    $state.go('dashboard');
                 });
         return myaccountsPromise;
      }
      
      function loadUserActiveCredits($state, ActiveCredit,$stateParams, logToServerUtil) {
         var activeCreditsPromise = ActiveCredit.userList({userId: $stateParams.id}).$promise;
         activeCreditsPromise
                 .then(function () {
                 }, function (reason) {
                    logToServerUtil.trace('get ActiveCredits failed', reason);
//                    $state.go('dashboard');
                 });
         return activeCreditsPromise;
      }

      function loadSingleUser($state, $stateParams, User, logToServerUtil) {
         var userPromise = User.get({id: $stateParams.id}).$promise;
         userPromise
                 .catch(function (reason) {
//                    logToServerUtil.trace('get User failed', reason);
//                    popupService.error('shared.popup.general-failure.title',
//                            'shared.popup.general-failure.body');
                    $state.go('dashboard');
                 });
         return userPromise;
      }
   }
})();
