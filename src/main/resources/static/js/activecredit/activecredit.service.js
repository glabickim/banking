(function () {
   'use strict';

   angular
           .module('bank')
           .factory('ActiveCredit', ActiveCredit);

   ActiveCredit.$inject = ['$resource', 'dateResolveUtil'];

   function ActiveCredit($resource, dateResolveUtil) {
      return $resource('/api/activeCredit/:id', {}, {
         update: {
            method: 'PUT'
         },
         query: {
            method: 'GET',
            isArray: true,
            transformResponse: transformResponse
         },
         get: {
            method: 'GET',
            transformResponse: transformResponse
         },
         refresh: {
            method: 'POST',
            url: '/api/activeCredit/refresh',
            isArray: false
         },
         logAction: {
            method: 'POST',
            url: '/api/log',
            isArray: false
         },
         finish : {
            method:'PUT',
            url: '/api/activeCredit/finish/:id',
            params:{id : '@id'}
         },
         delete:{
            method:'DELETE',
            url: '/api/activeCredit/delete/:id',
            params:{id : '@id'}
         },
         active: {method: 'GET',
            url: '/api/activeCredit/active',
            isArray: true,
            transformResponse: transformResponse
         },
         userList: {method: 'GET',
            url: '/api/activeCredit/user/:userId',
            isArray: true,
            transformResponse: transformResponse
         }
      });

      ////////////

      function transformResponse(data) {
         data = angular.fromJson(data);

         if ($.isArray(data)) {
            for (var i in data) {
               transformSingle(data[i]);
            }
         } else {
            transformSingle(data);
         }

         return data;
      }

      function transformSingle(activeCredit) {

         // naprawic daty
//          activeCredit.createdAt = dateResolveUtil.convertToDate(activeCredit.createdAt);
//         activeCredit.startDate = dateResolveUtil.convertToDate(activeCredit.startDate);
//         activeCredit.finishDate = dateResolveUtil.convertToDate(activeCredit.finishDate);
      }
   }
})();
