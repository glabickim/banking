package BANK.domain.account.dto;

import java.time.LocalDateTime;

/**
 *
 * @author Mateusz.Glabicki
 */
public class AccountSnapshot {

   private final Long id;
   private final Long currencyId;
   private final Long userId;

   private final boolean active;
   
   private final String iban;
   private final Double balance;
   private final LocalDateTime createdAt;

   public AccountSnapshot(Long id, String iban, Double balance, LocalDateTime createdAt,Long currencyId, boolean active, Long userId) {

      this.id = id;
      this.currencyId = currencyId;
      this.iban = iban;
      this.balance = balance;
      this.createdAt = createdAt;
      this.active = active;
      this.userId = userId;
   }

   public Long getId() {
      return id;
   }

   public Long getCurrencyId() {
      return currencyId;
   }

   public String getIban() {
      return iban;
   }
   
   public Long getUserId() {
      return userId;
   }

   public Double getBalance() {
      return balance;
   }

   public boolean isActive() {
      return active;
   }

   public LocalDateTime getCreatedAt() {
      return createdAt;
   }

   
}
