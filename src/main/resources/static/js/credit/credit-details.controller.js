(function () {
   'use strict';

   angular
           .module('bank')
           .controller('CreditDetailsCtrl', CreditDetailsCtrl);

   CreditDetailsCtrl.$inject = ['$scope', 'credit', 'currencys'];

   function CreditDetailsCtrl($scope, credit, currencys) {
      function _findCurrency(credit) {
         for (var i = 0; i < currencys.length; i++) {
            if (currencys[i].currencyId === credit.currencyId) {
               return currencys[i].symbol;
            }
         }
      }
      credit.currency = _findCurrency(credit);
      $scope.credit = credit;
      $scope.$parent.changeView('DETAILS');


   }
})();
