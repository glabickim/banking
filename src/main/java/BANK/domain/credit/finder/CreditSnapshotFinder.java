package BANK.domain.credit.finder;

import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.UUID;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;

import BANK.domain.credit.dto.CreditSnapshot;
import BANK.domain.credit.entity.Credit;
import BANK.domain.credit.repo.ICreditRepository;
import BANK.sharedkernel.annotations.Finder;

/**
 *
 * @author Mateusz.Glabicki
 */
@Finder
public class CreditSnapshotFinder
   implements ICreditSnapshotFinder {

    private final ICreditRepository creditRepository;

   @Autowired
   public CreditSnapshotFinder(ICreditRepository creditRepository) {
      this.creditRepository = creditRepository;
   }

   private List<CreditSnapshot> convert(List<Credit> credits) {
      return credits.stream()
         .map(Credit::toSnapshot)
         .collect(Collectors.toList());
   }

   @Override
   public CreditSnapshot findById(Long id) {
      Credit credit = creditRepository.findOne(id);
      return credit == null ? null : credit.toSnapshot();
   }
   
   @Override
   public List<CreditSnapshot> findAll() {
      List<Credit> credits = creditRepository.findAll();

      return convert(credits);
   }

   @Override
   public List<CreditSnapshot> findAll(Set<Long> ids) {
      List<Credit> credits = creditRepository.findAll(ids);

      return convert(credits);
   }

   @Override
   public List<CreditSnapshot> findActive() {
      return convert(creditRepository.findByActiveTrue());
   }
   
   @Override
   public Map<Long, CreditSnapshot> findAllAsMap(Set<Long> ids) {
      List<Credit> credits = creditRepository.findAll(ids);

      return credits
            .stream()
            .map(Credit::toSnapshot)
            .collect(Collectors.toMap(CreditSnapshot::getId, s ->s));
   }

}
