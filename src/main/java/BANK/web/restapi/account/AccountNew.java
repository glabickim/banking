/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package BANK.web.restapi.account;

import java.time.LocalDateTime;

import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotEmpty;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

import BANK.web.converter.LocalDateTimeDeserializer;
import BANK.web.restapi.annotations.RangeOfTimestamp;

/**
 *
 * @author Mateusz
 */
public class AccountNew {

   @NotNull
   private Long currencyId;

   @NotNull
   private String iban;

   @NotNull
   private Double balance;

   @NotNull
   private Long userId;

   public Long getUserId() {
      return userId;
   }

   public void setUserId(Long userId) {
      this.userId = userId;
   }

   public Long getCurrencyId() {
      return currencyId;
   }

   public void setCurrencyId(Long currencyId) {
      this.currencyId = currencyId;
   }

   public String getIban() {
      return iban;
   }

   public void setIban(String iban) {
      this.iban = iban;
   }

   public Double getBalance() {
      return balance;
   }

   public void setBalance(Double balance) {
      this.balance = balance;
   }

}
