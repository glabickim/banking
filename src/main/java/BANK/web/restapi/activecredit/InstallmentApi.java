package BANK.web.restapi.activecredit;

import java.security.Principal;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import BANK.domain.account.bo.IAccountBO;
import BANK.domain.account.dto.AccountSnapshot;
import BANK.domain.account.finder.IAccountSnapshotFinder;
import BANK.domain.credit.bo.IActiveCreditBO;
import BANK.domain.credit.dto.ActiveCreditSnapshot;
import BANK.domain.credit.dto.CreditSnapshot;
import BANK.domain.credit.finder.IActiveCreditSnapshotFinder;
import BANK.domain.credit.finder.ICreditSnapshotFinder;
import BANK.domain.installment.bo.IInstallmentBO;
import BANK.domain.installment.dto.InstallmentSnapshot;
import BANK.domain.installment.finder.IInstallmentSnapshotFinder;
import BANK.domain.transaction.bo.ITransactionBO;
import BANK.domain.user.dto.UserSnapshot;
import BANK.domain.user.finder.IUserSnapshotFinder;

@RestController
@RequestMapping("/api/installment")
public class InstallmentApi {

   private final IInstallmentSnapshotFinder installmentSnapshotFinder;

   private final IActiveCreditSnapshotFinder activeCreditSnapshotFinder;

   private final ICreditSnapshotFinder creditSnapshotFinder;

   private final IUserSnapshotFinder userSnapshotFinder;

   private final IInstallmentBO installmentBO;

   private final IActiveCreditBO activeCreditBO;

   private final IAccountSnapshotFinder accountSnapshotFinder;

   private final ITransactionBO transactionBO;

   private final IAccountBO accountBO;

   @Autowired
   public InstallmentApi(IInstallmentSnapshotFinder installmentSnapshotFinder, IActiveCreditBO creditBO,
      IInstallmentBO installmentBO, IUserSnapshotFinder userSnapshotFinder,
      IAccountSnapshotFinder accountSnapshotFinder, ITransactionBO transactionBO, IAccountBO accountBO,
      ICreditSnapshotFinder creditSnapshotFinder, IActiveCreditSnapshotFinder activeCreditSnapshotFinder) {
      this.installmentSnapshotFinder = installmentSnapshotFinder;
      this.installmentBO = installmentBO;
      this.userSnapshotFinder = userSnapshotFinder;
      this.activeCreditBO = creditBO;
      this.transactionBO = transactionBO;
      this.accountSnapshotFinder = accountSnapshotFinder;
      this.accountBO = accountBO;
      this.creditSnapshotFinder = creditSnapshotFinder;
      this.activeCreditSnapshotFinder = activeCreditSnapshotFinder;
   }

   @RequestMapping(method = RequestMethod.GET)
   public List<Installment> myInstallment(Principal principal) {

      UserSnapshot userSnapshot = userSnapshotFinder.findByUsername(principal.getName());

      List<InstallmentSnapshot> installmentSnapshots = installmentSnapshotFinder.findAll();

      return installmentSnapshots.stream()
         .map(Installment::new)
         .collect(Collectors.toList());
   }

   @RequestMapping(value = "/active",
      method = RequestMethod.GET)
   public List<Installment> active() {
      List<InstallmentSnapshot> installmentSnapshots = installmentSnapshotFinder.findActive();

      return installmentSnapshots.stream()
         .map(Installment::new)
         .collect(Collectors.toList());
   }

   @RequestMapping(value = "/{id}",
      method = RequestMethod.GET)
   public HttpEntity<Installment> get(@PathVariable("id") long id) {
      InstallmentSnapshot installmentSnapshot = installmentSnapshotFinder.findById(id);

      if (installmentSnapshot == null) {
         return new ResponseEntity<>(HttpStatus.NOT_FOUND);
      } else {
         return new ResponseEntity<>(new Installment(installmentSnapshot), HttpStatus.OK);
      }
   }

   @RequestMapping(value = "/credit/{id}",
      method = RequestMethod.GET)
   public List<Installment> getByCredit(@PathVariable("id") long id) {
      List<InstallmentSnapshot> installmentSnapshots = installmentSnapshotFinder.findByActiveCreditId(id);

      return installmentSnapshots.stream()
         .map(Installment::new)
         .collect(Collectors.toList());
   }

   @RequestMapping(value = "/pay/{id}",
      method = RequestMethod.PUT)
   public HttpEntity<Installment> pay(@PathVariable("id") Long installmentId, Principal principal) {
      UserSnapshot userSnapshot = userSnapshotFinder.findByUsername(principal.getName());

      InstallmentSnapshot installmentSnapshot = installmentSnapshotFinder.findById(installmentId);

      AccountSnapshot accountSnapshot = accountSnapshotFinder.findByActiveTrueAndUserId(userSnapshot.getId()).get(0);

      ActiveCreditSnapshot activeCreditSnapshot = activeCreditSnapshotFinder.findById(installmentSnapshot.getActiveCreditId());
      
      CreditSnapshot creditSnapshot = creditSnapshotFinder.findById(activeCreditSnapshot.getCreditId());
      
      if (accountSnapshot.getBalance() < installmentSnapshot.getAmount() || !accountSnapshot.getCurrencyId().equals(creditSnapshot.getCurrencyId())) {
         return new ResponseEntity<>(new Installment(installmentSnapshot), HttpStatus.NOT_ACCEPTABLE);
      }

      if (installmentId != null) {
         installmentBO.pay(installmentId);

         if (installmentSnapshotFinder.findByActiveTrueAndActiveCreditId(installmentId).isEmpty()) {
            activeCreditBO.finish(installmentSnapshot.getActiveCreditId());
         }

         transactionBO.add(accountSnapshot.getIban(), "1", installmentSnapshot.getAmount(),
            accountSnapshot.getCurrencyId());
         accountBO.boost(accountSnapshot.getId(), -installmentSnapshot.getAmount());

         return new ResponseEntity<>(new Installment(installmentSnapshot), HttpStatus.OK);
      }

      return new ResponseEntity<>(new Installment(installmentSnapshot), HttpStatus.NOT_FOUND);
   }

}
