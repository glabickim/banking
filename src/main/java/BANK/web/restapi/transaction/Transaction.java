package BANK.web.restapi.transaction;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.UUID;

import org.springframework.hateoas.ResourceSupport;

import BANK.domain.transaction.dto.TransactionSnapshot;

import com.fasterxml.jackson.annotation.JsonView;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.*;

/**
 *
 * @author Mateusz.Glabicki
 */

public class Transaction
   extends ResourceSupport
   implements Serializable {

   private static final long serialVersionUID = 123364444898230931L;

   private Long transactionId;

   private String fromIBAN;

   private String toIBAN;
   
   private Long currencyId;
   
   private Double value;
   
   private LocalDateTime createdAt;

   public Transaction(TransactionSnapshot transactionSnapshot) {
      this.transactionId = transactionSnapshot.getId();
      this.fromIBAN = transactionSnapshot.getFromIBAN();
      this.toIBAN = transactionSnapshot.getToIBAN();
      this.currencyId = transactionSnapshot.getCurrencyId();
      this.value = transactionSnapshot.getValue();
      this.createdAt = transactionSnapshot.getCreatedAt();
   }
   
   

   public Long getTransactionId() {
      return transactionId;
   }

   public String getFromIBAN() {
      return fromIBAN;
   }

   public String getToIBAN() {
      return toIBAN;
   }

   public Long getCurrencyId() {
      return currencyId;
   }

   public Double getValue() {
      return value;
   }

   public LocalDateTime getCreatedAt() {
      return createdAt;
   }
   
   
}
