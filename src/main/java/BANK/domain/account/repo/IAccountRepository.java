package BANK.domain.account.repo;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import BANK.domain.account.entity.Account;

/**
 *
 * @author Mateusz.Glabicki
 */
public interface IAccountRepository
   extends JpaRepository<Account, Long> {

   List<Account> findByIban(String iban);
   
   List<Account> findByActiveTrue();
   
   List<Account> findByUserId(Long userId);
   
   List<Account> findByActiveTrueAndUserId(Long userId);
}
