package BANK.web.restapi.user;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.UUID;

import org.springframework.hateoas.ResourceSupport;

import BANK.domain.user.dto.UserSnapshot;

import com.fasterxml.jackson.annotation.JsonView;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.*;

/**
 *
 * @author Mateusz.Glabicki
 */

public class User
   extends ResourceSupport
   implements Serializable {

   private static final long serialVersionUID = 123364444898230931L;

   private final Long userId;

   private final UUID guid;

   private final String title;

   private final String firstname;

   private final String lastname;

   private final String username;

   private final LocalDateTime createdAt;

   private final String mail;

   private final boolean enabled;

   private final LocalDateTime blockedAt;

   private final LocalDateTime updatedAt;

   private final long updatesCount;


   public User(UserSnapshot userSnapshot) {
      this.userId = userSnapshot.getId();
      this.guid = userSnapshot.getGuid();
      this.title = userSnapshot.getTitle();
      this.firstname = userSnapshot.getFirstname();
      this.lastname = userSnapshot.getLastname();
      this.username = userSnapshot.getUsername();
      this.createdAt = userSnapshot.getCreatedAt();
      this.mail = userSnapshot.getMail();
      this.enabled = userSnapshot.isEnabled();
      this.blockedAt = userSnapshot.getBlockedAt();
      this.updatedAt = userSnapshot.getUpdatedAt();
      this.updatesCount = userSnapshot.getVersion();
   }
   


   public Long getUserId() {
      return userId;
   }


   public UUID getGuid() {
      return guid;
   }


   public String getTitle() {
      return title;
   }


   public String getFirstname() {
      return firstname;
   }


   public String getLastname() {
      return lastname;
   }


   public String getUsername() {
      return username;
   }


   public LocalDateTime getCreatedAt() {
      return createdAt;
   }


   public String getMail() {
      return mail;
   }


   public boolean isEnabled() {
      return enabled;
   }


   public LocalDateTime getBlockedAt() {
      return blockedAt;
   }


   public LocalDateTime getUpdatedAt() {
      return updatedAt;
   }


   public long getUpdatesCount() {
      return updatesCount;
   }

   
}
