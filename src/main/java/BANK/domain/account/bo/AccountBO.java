package BANK.domain.account.bo;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.context.ApplicationEventPublisherAware;

import BANK.domain.account.dto.AccountSnapshot;
import BANK.domain.account.entity.Account;
import BANK.domain.account.repo.IAccountRepository;
import BANK.sharedkernel.annotations.BussinesObject;

@BussinesObject
public class AccountBO
   implements IAccountBO, ApplicationEventPublisherAware {

   private static final Logger LOGGER = LoggerFactory.getLogger(AccountBO.class);

   private final IAccountRepository accountRepository;

   private ApplicationEventPublisher eventPublisher;

   @Autowired
   public AccountBO(IAccountRepository accountRepository) {
      this.accountRepository = accountRepository;
   }

   @Override
   public void setApplicationEventPublisher(ApplicationEventPublisher applicationEventPublisher) {
      this.eventPublisher = applicationEventPublisher;
   }

   @Override
   public AccountSnapshot add(String iban, Double balance, Long currencyId, boolean active, Long userId) {
      Account account = new Account(iban, balance, currencyId,active,userId);
      account = accountRepository.save(account);

      AccountSnapshot accountSnapshot = account.toAccountSnapshot();

      LOGGER.info("Add Account <{}> <{}>",
         accountSnapshot.getId(), accountSnapshot.getIban());

      return accountSnapshot;
   }

   @Override
   public void delete(Long id) {
      Account account = accountRepository.findOne(id);
      accountRepository.delete(id);

      LOGGER.info("Delete Account <{}>", id);
   }
   
   @Override
   public AccountSnapshot boost(Long id, Double balance) {
      Account account = accountRepository.findOne(id);
      
      account.boost(balance);
      
      accountRepository.save(account);
      
      LOGGER.info("Boost Account <{}>", id);
      
      return account.toAccountSnapshot();
   }

   @Override
   public void active(Long id) {
      
      List<Account> accounts = accountRepository.findByActiveTrue();
      if(!accounts.isEmpty()){
         accounts.stream().map((acc) -> {
            acc.disable();
            return acc;
         }).forEach((acc) -> {
            accountRepository.save(acc);
         });
      }
      
      Account account = accountRepository.findOne(id);
      
      account.active();
      
      accountRepository.save(account);
      
      

      LOGGER.info("Active Account <{}>", id);
   }
   
   @Override
   public void edit(Long id, String iban, Double balance, Long currencyId) {
      Account account = accountRepository.findOne(id);

      AccountSnapshot accountSnapshot = account.toAccountSnapshot();

      account.editAccount(iban, balance,currencyId);
      accountRepository.save(account);

      LOGGER.info("Edit Account <{}> <{}>",
         accountSnapshot.getId(), accountSnapshot.getIban());
   }

}
