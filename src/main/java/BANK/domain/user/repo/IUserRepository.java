package BANK.domain.user.repo;

import java.util.List;
import java.util.UUID;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import BANK.domain.user.entity.User;

/**
 *
 * @author Mateusz.Glabicki
 */
public interface IUserRepository
   extends JpaRepository<User, Long> {

   List<User> findByUsernameIgnoreCase(String username);

   List<User> findByGuid(UUID guid);

   List<User> findByEnabledTrue();
   
   List<User> findByRole(String role);
   
   User findOneByUsernameAndPassword(String username,String password);


}
