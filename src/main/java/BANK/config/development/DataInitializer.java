package BANK.config.development;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import BANK.domain.account.finder.IAccountSnapshotFinder;
import BANK.domain.credit.finder.IActiveCreditSnapshotFinder;
import BANK.domain.credit.finder.ICreditSnapshotFinder;
import BANK.domain.currency.finder.ICurrencySnapshotFinder;
import BANK.domain.user.finder.IUserSnapshotFinder;
import BANK.sharedkernel.constant.Profiles;

@Component
@Profile(Profiles.DEVELOPMENT)
public class DataInitializer {

   @Autowired
   private IUserInitializer userInitializer;

   @Autowired
   private IUserSnapshotFinder userSnapshotFinder;
   
   @Autowired
   private ICurrencyInitializer currencyInitializer;
   
   @Autowired
   private ICurrencySnapshotFinder currencySnapshotFinder;
   
   @Autowired
   private IActiveCreditInitializer activeCreditInitializer;
   
   @Autowired
   private IActiveCreditSnapshotFinder activeCreditSnapshotFinder;

   @Autowired
   private ICreditInitializer creditInitializer;
   
   @Autowired
   private ICreditSnapshotFinder creditSnapshotFinder;
   
   @Autowired
   private IAccountInitializer accountInitializer;
   
   @Autowired
   private IAccountSnapshotFinder accountSnapshotFinder;
   
   @Transactional
   @PostConstruct
   public void init() {
      if (userSnapshotFinder.findAll().isEmpty()) {
         userInitializer.initalizer();
      }

      if (currencySnapshotFinder.findAll().isEmpty()) {
         currencyInitializer.initalizer();
      }

      if (accountSnapshotFinder.findAll().isEmpty()){
         accountInitializer.initalizer();
      }

      if (creditSnapshotFinder.findAll().isEmpty()){
         creditInitializer.initalizer();
      }

      if (activeCreditSnapshotFinder.findAll().isEmpty()){
         activeCreditInitializer.initalizer();
      }
      
   }

}
