package BANK.config.development;

import java.time.LocalDateTime;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import BANK.domain.currency.bo.ICurrencyBO;
import BANK.domain.user.bo.IUserBO;
import BANK.sharedkernel.constant.Profiles;

@Component
@Profile(Profiles.DEVELOPMENT)
public class CurrencyInitializer
   implements ICurrencyInitializer {

  @Autowired
   private ICurrencyBO currencyBO;

   @Transactional
   @Override
   public void initalizer() {

         currencyBO.add("PLN");
         
         currencyBO.add("EUR");
         
         currencyBO.add("USD");
      
   }

}
