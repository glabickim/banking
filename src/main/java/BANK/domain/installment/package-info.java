/**
 * This domain is concentrated on installment management. It allows to access installment data through REST API and manage
 * them in the database.
 */
package BANK.domain.installment;
