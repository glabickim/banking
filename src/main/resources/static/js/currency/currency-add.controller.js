(function () {
   'use strict';

   angular
           .module('bank')
           .controller('CurrencyAddCtrl', CurrencyAddCtrl);

   CurrencyAddCtrl.$inject = [
      '$scope',
      '$state',
      'Currency',
      'CommonUtilService',
      'logToServerUtil',
      'popupService',
      'languageUtil'];

   function CurrencyAddCtrl($scope, $state, Currency, CommonUtilService, logToServerUtil, popupService, languageUtil) {
      CommonUtilService.initCommonAndOperator($scope);


      $scope.currency = {
         symbol: ""
      };

      $scope.saveCurrency = saveCurrency;

      function saveCurrency() {
         var currency = new Currency();
         currency.symbol = $scope.currency.symbol;

         currency.$save()
                 .then(function (result) {
                    popupService.success('currency-add.popup.save.success.title', 'currency-add.popup.save.success.body');
                    $state.go('currency.list');
                 }, function (reason) {
                    popupService.error('currency-add.popup.save.failure.title', 'currency-add.popup.save.failure.body');
                    logToServerUtil.trace('Save Currency failure', reason);
                 });
      }
   }
})();