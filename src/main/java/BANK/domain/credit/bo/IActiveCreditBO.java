package BANK.domain.credit.bo;

import java.time.LocalDateTime;
import java.util.UUID;

import BANK.domain.credit.dto.ActiveCreditSnapshot;
import BANK.domain.credit.dto.CreditSnapshot;

/**
 *
 * @author Mateusz.Glabicki
 */
public interface IActiveCreditBO {

   ActiveCreditSnapshot add(Long creditId, Long userId, LocalDateTime finishDate , Double value);

//   CreditSnapshot edit(Long id, LocalDateTime startDate, LocalDateTime finishDate,
//      Long provision, Long installment, Double rateOfInterest, Double interest, boolean insurance);
//
//   void delete(Long creditId);

   void finish(Long activeCreditId);
}
