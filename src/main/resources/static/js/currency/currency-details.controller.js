(function () {
   'use strict';

   angular
           .module('bank')
           .controller('CurrencyDetailsCtrl', CurrencyDetailsCtrl);

   CurrencyDetailsCtrl.$inject = ['$scope', 'currency'];

   function CurrencyDetailsCtrl($scope, currency) {
      $scope.currency = currency;
      $scope.$parent.changeView('DETAILS');
   }
})();
