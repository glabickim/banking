package BANK.domain.credit.entity;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.UUID;

import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Version;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotEmpty;

import BANK.config.persistance.converter.LocalDateTimePersistenceConverter;
import BANK.domain.credit.dto.CreditSnapshot;
import BANK.sharedkernel.exception.EntityInStateNewException;

/**
 *
 * @author Mateusz.Glabicki
 */
@Entity
public class Credit
   implements Serializable {

   private static final long serialVersionUID = 7972265007575707207L;

   @Id
   @GeneratedValue(strategy = GenerationType.AUTO)
   private Long id;

   @NotNull
   @Convert(converter = LocalDateTimePersistenceConverter.class)
   private LocalDateTime createdAt;

   @Version
   private long version;

   // prowizja w %
   @NotNull
   private Long provision;

   // odsetki w %
   @NotNull
   private Long interest;

   // co ile rata
   @NotNull
   private Long installment;

   // minimalna kwota ktora pozwala na pobranie kredytu
   @NotNull
   private Double contribution;

   // wartosc kapitalu
   @NotNull
   private Double minValue;

   // wartosc kapitalu
   @NotNull
   private Double maxValue;

   //zmienne raty - 0 stałe, 1 zmienne
   private boolean changingRate;

   private boolean active;

   //ubezpieczenie +2%
   private boolean insurance;

   @NotNull
   private String type;

   @NotNull
   private Long currencyId;

   protected Credit() {
   }

   public Credit(Long provision, Long interest, Long installment, Double contribution, boolean changingRate,
      boolean active, boolean insurance, Double minValue, Double maxValue, String type, Long currencyId) {
      this.provision = provision;
      this.interest = interest;
      this.installment = installment;
      this.contribution = contribution;
      this.changingRate = changingRate;
      this.active = active;
      this.insurance = insurance;
      this.version = 1;
      this.createdAt = LocalDateTime.now();
      this.minValue = minValue;
      this.maxValue = maxValue;
      this.type = type;
      this.currencyId = currencyId;
   }

   public void finish() {
      this.active = false;
   }

   public CreditSnapshot toSnapshot() {
      if (id == null) {
         throw new EntityInStateNewException();
      }

      return new CreditSnapshot(id, createdAt, version, provision, interest, installment, contribution, changingRate,
         active, insurance, minValue, maxValue, type, currencyId);
   }

   public void editCredit(Long provision, Long interest, Long installment, Double contribution, boolean changingRate,
      boolean insurance, Double minValue, Double maxValue, String type, Long currencyId) {
      this.provision = provision;
      this.installment = installment;
      this.changingRate = changingRate;
      this.interest = interest;
      this.insurance = insurance;
      this.contribution = contribution;
      this.minValue = minValue;
      this.maxValue = maxValue;
      this.type = type;
      this.currencyId = currencyId; 
   }

   Long getId() {
      return id;
   }
}
