package BANK.web.controller;

import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class UserController {

  @RequestMapping("/user/list")
   public String list() {
      return "user/list";
   }

   @RequestMapping("/user/common")
   public String common() {
      return "user/common";
   }

   @RequestMapping("/user/details")
   public String detail() {
      return "user/details";
   }
   
   @RequestMapping("/user/add")
   public String add() {
      return "user/add";
   }
   
   @RequestMapping("/user/edit")
   public String edit() {
      return "user/edit";
   }

}
