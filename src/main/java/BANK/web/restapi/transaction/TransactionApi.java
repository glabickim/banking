package BANK.web.restapi.transaction;

import java.security.Principal;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Set;
import java.util.UUID;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import BANK.domain.transaction.dto.TransactionSnapshot;
import BANK.domain.transaction.finder.ITransactionSnapshotFinder;

import com.fasterxml.jackson.annotation.JsonView;

import BANK.domain.transaction.bo.ITransactionBO;

@RestController
@RequestMapping("/api/transaction")
public class TransactionApi {

   
   private final ITransactionSnapshotFinder transactionSnapshotFinder;

   private final ITransactionBO transactionBO;

   @Autowired
   public TransactionApi(ITransactionSnapshotFinder transactionSnapshotFinder, ITransactionBO transactionBO) {
      this.transactionSnapshotFinder = transactionSnapshotFinder;
      this.transactionBO = transactionBO;
   }


   @RequestMapping(method = RequestMethod.GET)
   public List<Transaction> list() {
      List<TransactionSnapshot> transactionSnapshots = transactionSnapshotFinder.findAll();

      return transactionSnapshots.stream()
         .map(Transaction::new)
         .collect(Collectors.toList());
   }

}
