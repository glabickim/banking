(function () {
   'use strict';

   angular
           .module('bank')
           .controller('CreditEditCtrl', CreditEditCtrl);

   CreditEditCtrl.$inject = [
      '$scope',
      '$state',
      'currencys',
      'credit',
      'Credit',
      'CommonUtilService',
      'logToServerUtil',
      'popupService'];

   function CreditEditCtrl($scope, $state, currencys, credit, Credit, CommonUtilService, logToServerUtil, popupService) {
      CommonUtilService.initCommonAndOperator($scope);

      $scope.currencys = currencys;

      $scope.credit = {
         creditId: credit.creditId,
         changingRate: credit.changingRate,
         interest: credit.interest,
         insurance: credit.insurance,
         installment: credit.installment,
         provision: credit.provision,
         contribution: credit.contribution,
         maxValue: credit.maxValue,
         minValue: credit.minValue,
         type: credit.type,
         currencyId: credit.currencyId
      };
      $scope.saveCredit = saveCredit;

      function saveCredit() {
         var credit = new Credit();
         credit.changingRate = $scope.credit.changingRate;
         credit.interest = $scope.credit.interest;
         credit.insurance = $scope.credit.insurance;
         credit.installment = $scope.credit.installment;
         credit.provision = $scope.credit.provision;
         credit.contribution = $scope.credit.contribution;
         credit.maxValue = $scope.credit.maxValue;
         credit.minValue = $scope.credit.minValue;
         credit.creditId = $scope.credit.creditId;
         credit.type = $scope.credit.type;
         credit.currencyId = $scope.credit.currencyId;

         credit.$update()
                 .then(function (result) {
                    popupService.success('credit-edit.popup.save.success.title', 'credit-edit.popup.save.success.body');
                    $state.go('credit.list');
                 }, function (reason) {
                    popupService.error('credit-edit.popup.save.failure.title', 'credit-edit.popup.save.failure.body');
                    logToServerUtil.trace('Credit Template failure', reason);
                 });
      }
   }
})();