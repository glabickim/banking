(function () {
   'use strict';

   angular
           .module('bank')
           .factory('Transaction', Transaction);

   Transaction.$inject = ['$resource', 'dateResolveUtil'];

   function Transaction($resource, dateResolveUtil) {
      return $resource('/api/transaction/:id', {}, {
         update: {
            method: 'PUT'
         },
         query: {
            method: 'GET',
            isArray: true,
            transformResponse: transformResponse
         },
         get: {
            method: 'GET',
            transformResponse: transformResponse
         },
         refresh: {
            method: 'POST',
            url: '/api/transaction/refresh',
            isArray: false
         },
         delete:{
            method:'DELETE',
            url: '/api/transaction/delete/:id',
            params:{id : '@id'}
         }
      });

      ////////////

      function transformResponse(data) {
         data = angular.fromJson(data);

         if ($.isArray(data)) {
            for (var i in data) {
               transformSingle(data[i]);
            }
         } else {
            transformSingle(data);
         }

         return data;
      }

      function transformSingle(transaction) {

         // naprawic daty
//          transaction.createdAt = dateResolveUtil.convertToDate(transaction.createdAt);
//         transaction.startDate = dateResolveUtil.convertToDate(transaction.startDate);
//         transaction.finishDate = dateResolveUtil.convertToDate(transaction.finishDate);
      }
   }
})();
