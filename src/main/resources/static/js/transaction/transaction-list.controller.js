(function () {
   'use strict';

   angular
           .module('bank')
           .controller('TransactionListCtrl', TransactionListCtrl);

   TransactionListCtrl.$inject = [
      '$scope',
      'Transaction',
      'transactions',
      '$stateParams',
      '$location',
      '$timeout',
      'dateResolveUtil'];

   function TransactionListCtrl($scope, Transaction, transactions, $stateParams, $location, $timeout,dateResolveUtil) {
      $scope.loading = false;
      $scope.transactions = transactions;
      $scope.onlyDate = dateResolveUtil.onlyDate;
      
   }
})();